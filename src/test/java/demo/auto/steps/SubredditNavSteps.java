package demo.auto.steps;

import demo.auto.pages.HomePage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

import static org.junit.Assert.assertTrue;

/**
 * Created by Saurabh on 18/08/2019.
 */
public class SubredditNavSteps extends ScenarioSteps {



    HomePage redditHomePage;

    @Step
    public void selectSubreddit(String subredditName)
    {
        redditHomePage.getTopSubredditNav().selectSubredditFromTopQuickNav(subredditName);
    }

    @Step
    public void verifyNavigatedToSelectedSubreddit(String subredditName)
    {
        assertTrue(redditHomePage.getTopSubredditNav().isSubredditSelected(subredditName));
    }

    @Step
    public void userSelectsTabMenu(String tabMenuOption)
    {
        redditHomePage.getTabMenu().selectTabMenu(tabMenuOption);
    }

    @Step
    public void verifyResultsUpdatedAsPerFilter(String optionName)
    {
        assertTrue(redditHomePage.getTabMenu().isOptionSelected(optionName));
    }




}
