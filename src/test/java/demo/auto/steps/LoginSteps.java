package demo.auto.steps;

import cucumber.api.DataTable;
import demo.auto.pages.HomePage;
import demo.auto.pages.LoginPage;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.List;
import java.util.Map;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class LoginSteps extends ScenarioSteps{


    LoginPage redditLoginPage;
    HomePage redditHomePage;

    @Step
    public void verifyUserOnRedditPage()
    {
        redditLoginPage.open();
    }



    @Step
    public void userLogsIn(String username, String password)
    {
        redditLoginPage.getUserNameField().sendKeys(username);
        redditLoginPage.getPasswordField().sendKeys(password);
        redditLoginPage.getRememberMeCheckbox().click();
        redditLoginPage.getLoginButton().click();
    }

    @Step
    public void verifyUserOnHomePage(String username) throws InterruptedException {
        Thread.sleep(2000);
        System.out.println(redditHomePage.getUserNamePlaceholder().getText().toUpperCase());
        System.out.println(username.toUpperCase());
        assertTrue(redditHomePage.getUserNamePlaceholder().getText().toUpperCase().equals(username.toUpperCase()));

    }

    @Step
    public void userSelectsSubredditFromDropdown(String subredditName)
    {
        redditLoginPage.getSubredditDropdown().click();
        List<WebElement> options = redditLoginPage.getSubredditDropdownList();
        for(int i=0;i<options.size();i++)
        {
            System.out.println(options.get(i).getText());
            if(options.get(i).getText().equalsIgnoreCase(subredditName))
            {
                options.get(i).click();
                break;
            }
        }

    }

    @Step
    public void verifySubredditName(String subredditName)
    {
        assertTrue(redditLoginPage.getSubredditNamePlaceholder().getText().equalsIgnoreCase(subredditName));
    }


    @Step
    public void userProvidePreferences(DataTable dt)
    {
        redditHomePage.getPreferencesLink().click();
        List<List<String>> data = dt.raw();
        redditHomePage.getPreferencesOptionsParent().selectCheckBoxOrRadioButtonForSection(data.get(1).get(0));
        redditHomePage.getPreferencesOptionsParent().selectCheckBoxOrRadioButtonForSection(data.get(1).get(1));
        redditHomePage.getPreferencesOptionsParent().selectCheckBoxOrRadioButtonForSection(data.get(1).get(2));
        redditHomePage.getPreferencesOptionsParent().selectCheckBoxOrRadioButtonForSection(data.get(1).get(3));

    }


    @Step
    public void userSavePreferences()
    {
        redditHomePage.getSavePreferencesButton().click();
    }



}