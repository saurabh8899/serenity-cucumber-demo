package demo.auto.pages;

import demo.auto.components.Preferences;
import demo.auto.components.TabMenu;
import demo.auto.components.TopSubredditQuickNav;
import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;


public class HomePage extends PageObject {

    @FindBy(xpath="//span[@class='user']/a")
    private WebElement userNamePlaceholder;

    @FindBy(xpath="//a[@class='pref-lang choice']")
    private WebElement preferencesLink;

    @FindBy(xpath="//input[@value='save options']")
    private WebElement savePreferencesButton;

    @FindBy(xpath="//table[@class='content preftable']")
    private WebElement preferencesOptionsParent;

    @FindBy(xpath="//ul[@class='flat-list sr-bar hover'][2]")
    private WebElement topSubredditNav;

    @FindBy(xpath="//ul[@class='tabmenu ']")
    private WebElement tabMenu;



    public WebElement getPreferencesLink()
    {
        return preferencesLink;
    }

    public WebElement getUserNamePlaceholder()
    {
        return userNamePlaceholder;
    }

    public WebElement getSavePreferencesButton(){
        return savePreferencesButton;
    }

    public Preferences getPreferencesOptionsParent(){return new Preferences(preferencesOptionsParent); }

    public TopSubredditQuickNav getTopSubredditNav(){return new TopSubredditQuickNav(topSubredditNav); }

    public TabMenu getTabMenu(){return new TabMenu(tabMenu); }



}
