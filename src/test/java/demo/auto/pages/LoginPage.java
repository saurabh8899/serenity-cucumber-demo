package demo.auto.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.List;

/**
 * Created by Saurabh on 8/08/2019.
 */
@DefaultUrl("https://www.old.reddit.com/")
public class LoginPage extends PageObject {

    @FindBy(name="user")
    private WebElement userNameField;

    @FindBy(name="passwd")
    private WebElement passwordField;

    @FindBy(xpath="//div[@class='submit']/descendant::button[@class='btn']")
    private WebElement loginButton;

    @FindBy(xpath="//div[@id='sr-header-area']/descendant::div[@class='dropdown srdrop']/span")
    private WebElement  subredditDrowdown;

    @FindBy(xpath="//div[@class='drop-choices srdrop inuse']")
    private WebElement subredditdropdownlist;

    @FindBy(xpath="//span[@class='hover pagename redditname']/a")
    private WebElement subredditNamePlaceholder;

    @FindBy(xpath="//div[@id='remember-me']/input")
    private WebElement rememberMeCheckbox;




    public WebElement getUserNameField()
    {
        return userNameField;
    }

    public WebElement getPasswordField()
    {
        return passwordField;
    }

    public WebElement getLoginButton()
    {
        return loginButton;
    }

    public WebElement getSubredditDropdown()
    {
        return subredditDrowdown;
    }

    public List<WebElement> getSubredditDropdownList()
    {
        return subredditdropdownlist.findElements(By.xpath(".//a"));
    }

    public WebElement getSubredditNamePlaceholder()
    {
        return subredditNamePlaceholder;
    }

    public WebElement getRememberMeCheckbox()
    {
        return rememberMeCheckbox;
    }


}
