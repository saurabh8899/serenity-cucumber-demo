package demo.auto.components;

import org.apache.xpath.operations.Bool;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Saurabh on 18/08/2019.
 */
public class TopSubredditQuickNav {

    private WebElement parent;


    public TopSubredditQuickNav(WebElement parent){
        this.parent = parent;
    }




    public List<WebElement> getAllSubredditOptions(){
        return parent.findElements(By.xpath("./li/a"));
        }



    public void selectSubredditFromTopQuickNav(String subredditName) {
        for (int i = 0; i < getAllSubredditOptions().size(); i++) {
            System.out.println(getAllSubredditOptions().get(i).getText());
            if (getAllSubredditOptions().get(i).getText().equalsIgnoreCase(subredditName)) {
                getAllSubredditOptions().get(i).click();
                break;
            }
        }
    }

    public Boolean isSubredditSelected(String subredditName) {
        Boolean flag = false;
        for (int i = 0; i < getAllSubredditOptions().size(); i++) {
            if (getAllSubredditOptions().get(i).getText().equalsIgnoreCase(subredditName)) {
                flag =  getAllSubredditOptions().get(i).findElement(By.xpath("./ancestor::li")).getAttribute("class").equalsIgnoreCase("Selected");
                break;
            }
        }
        return flag;
    }

}




