package demo.auto.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by Saurabh on 10/08/2019.
 */
public class Preferences {

    private WebElement parent;


    public Preferences(WebElement parent){
        this.parent = parent;
    }


    public List<WebElement> getAllCheckboxAndRadioButtonPreferences(){
        return parent.findElements(By.xpath("./tbody/tr/td//label"));
    }


    public void selectCheckBoxOrRadioButtonForSection(String sectionName){
        for(int i=0; i<getAllCheckboxAndRadioButtonPreferences().size(); i++)
        {
            if(getAllCheckboxAndRadioButtonPreferences().get(i).getText().equalsIgnoreCase(sectionName))
            {
                getAllCheckboxAndRadioButtonPreferences().get(i).click();
                break;
            }
        }
    }





}
