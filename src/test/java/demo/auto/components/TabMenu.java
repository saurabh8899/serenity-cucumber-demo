package demo.auto.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class TabMenu {

    private WebElement parent;


    public TabMenu(WebElement parent){
        this.parent = parent;
    }


    public List<WebElement> getAllTabMenuOptions(){
        return parent.findElements(By.xpath("./li/a"));
    }


    public void selectTabMenu(String tabMenuName){
        for(int i=0; i<getAllTabMenuOptions().size(); i++)
        {
            if(getAllTabMenuOptions().get(i).getText().equalsIgnoreCase(tabMenuName))
            {
                getAllTabMenuOptions().get(i).click();
                break;
            }
        }
    }

    public Boolean isOptionSelected(String optioName){
        Boolean flag = false;
        for(int i=0; i<getAllTabMenuOptions().size(); i++)
        {
            if(getAllTabMenuOptions().get(i).getText().equalsIgnoreCase(optioName))
            {
                getAllTabMenuOptions().get(i).findElement(By.xpath("./parent::li")).getAttribute("class").equalsIgnoreCase("selected");
                flag=true;
                break;
            }
        }
        return flag;
    }





}
