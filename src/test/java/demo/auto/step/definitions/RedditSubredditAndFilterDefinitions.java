package demo.auto.step.definitions;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import demo.auto.steps.LoginSteps;
import demo.auto.steps.SubredditNavSteps;
import net.thucydides.core.annotations.Steps;

public class RedditSubredditAndFilterDefinitions {

    @Steps
    SubredditNavSteps steps;

    private static String tabMenuOption;



    @When("^the user selects '(.*)' subreddit$")
    public void userSelectsSubreddit(String subredditName) {
        steps.selectSubreddit(subredditName);
    }

    @Then("^the user should be navigated to '(.*)' subreddit$")
    public void verifyNavigationToSubreddit(String subredditName) {
        steps.verifyNavigatedToSelectedSubreddit(subredditName);
    }


    @When("^the user selects '(.*)' tab menu from top navigation bar$")
    public void userSelectsTabMenu(String tabMenuOption) {
        steps.userSelectsTabMenu(tabMenuOption);
        this.tabMenuOption = tabMenuOption;

    }

    @Then("^the selected tab menu option should appear highlighted$")
    public void verifyResultsUpdatedAsPerFilter() {
        steps.verifyResultsUpdatedAsPerFilter(tabMenuOption);
    }


}
