package demo.auto.step.definitions;

import cucumber.api.DataTable;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import demo.auto.steps.LoginSteps;
import net.thucydides.core.annotations.Steps;

public class RedditLoginDefinitions {

    @Steps
    private LoginSteps redditSteps;
    private static String username;
    private String subredditName;



    @Given("^the user is on reddit website$")
    public void verifyUserOnRedditPage() {
        redditSteps.verifyUserOnRedditPage();

    }

    @When("^user logs in to the website with username '(.*)' and password '(.*)'$")
    public void userlogsIn(String username, String password) {
        redditSteps.userLogsIn(username, password);
        this.username = username;

    }

    @Given("^the user has logged onto reddit website$")

    public void verifyUserNameOnHomePage() throws InterruptedException {
        redditSteps.verifyUserOnHomePage(username);
    }

    @Then("^he should see the username on the home page$")
    public void verifyUserNameOnHomePageAgain() throws InterruptedException {
        redditSteps.verifyUserOnHomePage(username);
    }


    @Given("^user fills the details as below preferences$")
    public void userProvidePreferences(DataTable dt) {
        redditSteps.userProvidePreferences(dt);
    }


    @Given("^user selects '(.*)' subreddit from the dropdown$")
    public void userSelectsSubredditFromDropdown(String subredditName) {
        redditSteps.userSelectsSubredditFromDropdown(subredditName);
        this.subredditName=subredditName;

    }

    @Then("^the user should see the selected subreddit$")
    public void verifySubredditName() {
        redditSteps.verifySubredditName(subredditName);
    }


    @Then("^user should be able to save the preferences$")
    public void userSavePreferences() {
        redditSteps.userSavePreferences();
    }


}
