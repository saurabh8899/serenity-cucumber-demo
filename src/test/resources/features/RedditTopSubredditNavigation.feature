Feature: Reddit top subreddit navigation bar testing

  Scenario: User logs into Reddit.com
    Given the user is on reddit website
    When user logs in to the website with username 'SerenityTesting' and password 'serenity'
    Then he should see the username on the home page


  Scenario: User chooses the subreddit from top navigation bar
    Given the user has logged onto reddit website
    When the user selects 'Jokes' subreddit
    Then the user should be navigated to 'Jokes' subreddit


  Scenario: User chooses filter for the selected subreddit
    Given the user has logged onto reddit website
    When the user selects 'gilded' tab menu from top navigation bar
    Then the selected tab menu option should appear highlighted