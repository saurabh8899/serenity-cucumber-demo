Feature: Login to Reddit

  Scenario: User logs into Reddit.com
    Given the user is on reddit website
    When user logs in to the website with username 'SerenityTesting' and password 'serenity'
    Then he should see the username on the home page


  Scenario: User chooses the preferences
    Given the user has logged onto reddit website
    And user fills the details as below preferences
      |beta options|media|link options|privacy options|
      |I would like to beta test features for reddit|Show thumbnails next to links|compress the link display|allow reddit to log my outbound clicks for personalization|
    Then user should be able to save the preferences


  Scenario: User selects subreddit from the left hand side dropdown
    Given the user has logged onto reddit website
    And user selects 'Gaming' subreddit from the dropdown
    Then the user should see the selected subreddit