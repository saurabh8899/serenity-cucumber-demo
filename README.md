# Demo serenity-cucumber automation framework with java

The framework is a serenity-cucumber based BDD framework written in Java and has 2 cucumber feature files with 3 scenarios each. The scenario automates some of the funtionalities of social networking site Reddit.com. The framework uses the page object design pattern and is divided into four layers for ease of development and maintenance.

    1. Feature file
    2. Step definitions file
    3. Steps file
    4. Page file

The framework also uses reusable components for complex functionalities in the AUT which abstracts the behaviour in separate Java classes and helps in reusability and writing tests easily. 

## Prerequisite

1. Make sure Java and Maven has been configured on the system where the test would be executed.
2. Before executing the tests make sure webdriver-manager is installed and up and listening on port 4444 on local machine. The tests were run on webdriver-manager version 12.1.6. Webdriver-manager can be installed using npm package manager.
3. The test is designed to run on chrome by default and were run on near latest version of chrome 76.0.3809.100. Tests can also be made to run on firefox from the MyRemoteDriver class by simple setting up desired capabilities to firefox in MyRemoteDriver class.
4. The tests have been configured to run parallaly in two browser instances. The number of browser instances can be changed in the pom.xml file under filter <parallel.tests>.

## Executing the test

To run the demo project, clone it and use below maven command from the command line in the project directory.

```java
mvn clean verify serenity:aggregate
```

## Features
1. BDD framework which provides living documentation
2. Cucumber allows variable data to be inserted for the same scenario in the form of data tables for flexible tests using scenario outline
3. The framework can be easily modified to run tests in parallel by modifying fork counts in the pom.xml file.
4. Very detailed report generated with screenshots are provided for each step of the tests and can be modified easily to capture screenshots only on failure.
5. Various Java libraries like pom.xml makes the framework flexible to extend frameworks to feed data from excel sheets.
6. Can be easily made to run on docker containers
7. serenity.properties file allows user to input various important parameters.

## Report
The final report will be generated on the following path
```bash
\\serenitycucumberdemo\target\site\serenity\index.html
```
Below are few screenshots of the report

![image](https://bitbucket.org/saurabh8899/serenity-cucumber-demo/raw/3b47a94312a32f94a3767cfd487f33c1327d6073/src/test/java/demo/auto/example/report_dashboard.png)
![image](https://bitbucket.org/saurabh8899/serenity-cucumber-demo/raw/3b47a94312a32f94a3767cfd487f33c1327d6073/src/test/java/demo/auto/example/features.png)
![image](https://bitbucket.org/saurabh8899/serenity-cucumber-demo/raw/3b47a94312a32f94a3767cfd487f33c1327d6073/src/test/java/demo/auto/example/screenshots.png)


